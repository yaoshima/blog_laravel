<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Blog extends Model
{	
	protected $fillable = ['title', 'body'];

	public function likes(){
		return $this->hasMany('App\Like');
	}

	public function tags(){
		return $this->belongsToMany('App\Tag');
	}

	private function seedBlog($session){
		$blogs = [
			['title' => 'Learning laravel', 'body' => 'So excited learning laravel'],
			['title' => 'Heize concert', 'body' => 'Must buy the ticket now'],
			['title' => 'Cryptocurrency scam?', 'body' => 'Economist have doubt on bitcoin']
		];
		$session->put('blogs',$blogs);
	}

	public function getAllBlog($session){
		if($session->has('blogs')){
			return $session->get('blogs');
		}
		else{
			$this->seedBlog($session);
		}
	}

	public function getBlogById($session, $id){
		if($session->has('blogs')){
			$blogs = $session->get('blogs');
			return $blogs[$id];
		}
		else{
			seedBlog();
		}
	}

	public function updateBlog($session, $id, $title, $body){
		if($session->has('blogs')){
			$blogs = $session->get('blogs');

			$blogs[$id]['title'] = $title;
			$blogs[$id]['body'] = $body;

			$session->put('blogs', $blogs);

		}
		else{
			seedBlog();
		}
	}

	public function insertBlog($session, $title, $body){
		if($session->has('blogs')){
			$blogs = $session->get('blogs');
			$blog = [
				'title' => $title,
				'body' => $body
			];
			array_push($blogs, $blog);
			$session->put('blogs',$blogs);

		}
		else{
			seedBlog();
		}
	}

	public function getTitleAttribute($title){
		return ucwords($title);
	}

	public function setTitleAttribute($title){
		$this->attributes['title'] = strtolower($title);
	}
}