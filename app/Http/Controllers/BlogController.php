<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Session\Store;
use App\BLog;
use App\Like;
use App\Tag;

class BlogController extends Controller
{
    public function index(){
    	//$blogs = Blog::orderBy('created_at', 'desc')->get();
        $blogs = Blog::orderBy('created_at','desc')->paginate(1);
    	return view('blogs.index',['blogs' => $blogs]);
    }

    public function adminIndex(){
        $blogs = Blog::all();
        return view('admin.index',['blogs' => $blogs]);
    }

    public function show($id){
    	$blog = Blog::find($id);
    	return view('blogs.show', ['blog' => $blog]);
    }

    public function new(){
        $blog = new Blog();
        $tags = Tag::all();
        return  view('admin.create', ['blog' => $blog, 'tags' => $tags]);
    }

    public function create(Request $request){        
        $this->validate($request, [
                'title' => 'required',
                'body' => 'required'
            ]);
        $blog = new Blog(['title' => $request->input('title'), 'body' => $request->input('body')]);
        $blog->save();
        $tags = $request->input('tags') === null ? [] : $request->input('tags');
        $blog->tags()->attach($tags);
        return redirect()->route('admin.index')->with('title','the new title is : '.$request->input('title'));

    }

    public function edit($id){
        $blog = Blog::find($id);
        $tags = Tag::all();
        return view('admin.edit', ['blog' => $blog, 'tags' => $tags]);
    }

    public function update(Request $request){
        $blog = Blog::find($request->input('id'));
        $this->validate($request, [
                'title' => 'required',
                'body' => 'required',
                'id' => 'required'
            ]);
        $blog->title = $request->input('title');
        $blog->body = $request->input('body');
        $tags = $request->input('tags') === null ? [] : $request->input('tags');
        $blog->tags()->sync($tags);
        $blog->save();
        return redirect()->route('admin.index')->with('title','the updated title is : '. $request->input('title'));
    }

    public function destroy($id){
        $blog = Blog::find($id);
        $blog->likes()->delete();
        $blog->tags()->detach();
        $blog->delete();
        return redirect()->route('admin.index')->with('title', $blog->title.' is deleted');
    }

    public function createLike($id){
        $like = new Like();
        $blog = Blog::find($id);
        $blog->likes()->save($like);
        return redirect()->back();
    }
}
