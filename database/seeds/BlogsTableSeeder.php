<?php

use Illuminate\Database\Seeder;
use App\Blog;

class BlogsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $blog = new Blog(['title' => 'Suju concert in JKT!', 'body' => 'Elf is so happy']);
        $blog->save();

        $blog = new Blog(['title' => 'Bitcoin is crash', 'body' => 'Whales start selling their bitcoin']);
        $blog->save();
    }
}
