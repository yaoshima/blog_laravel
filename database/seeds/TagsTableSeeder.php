<?php

use Illuminate\Database\Seeder;
use App\Tag;

class TagsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $tag = new Tag(['name' => 'Tutorial']);
        $tag->save();

        $tag = new Tag(['name' => 'Random']);
        $tag->save();
    }
}
