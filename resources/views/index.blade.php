@extends('layout.master')

@section('content')
    
    <div class="container">
        <h1 class="fancy-title my-4">Media Indonesia</h1>
        <div class="row text-md-left text-center">
            <div class="col-12">
                <h2 class="text-secondary">Some Title</h2>
                <p class="text-truncate"">SSuspendisse sit amet tempor lacus. Quisque posuere sit amet ante eget commodo. Interdum et malesuada fames ac ante ipsum primis in faucibus. Nam viverra sapien et faucibus tincidunt. Donec felis nibh, pharetra in mi in, placerat facilisis nulla. Curabitur dapibus facilisis elementum. Vestibulum blandit sollicitudin orci et ornare. Sed pulvinar molestie consectetur. Donec condimentum mi ut tempus hendrerit. Pellentesque vel placerat ex. Phasellus augue ex, accumsan sed venenatis id, vestibulum vel urna. Suspendisse potenti. Suspendisse id viverra leo, eu euismod diam. Nullam laoreet dui in euismod faucibus.</p>
                <a href="">Read more...</a>
            </div>
        </div>
        <hr>
        <div class="row text-md-left text-center">
            <div class="col-12">
                <h2 class="text-secondary">Some Title</h2>
                <p>Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit...</p>
                <a href="">Read more...</a>
            </div>
        </div>
    </div>
       
@endsection