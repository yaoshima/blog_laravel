@if(count($errors->all()))
	<div class="alert alert-success" role="alert">
		<ul>
			@foreach($errors->all() as $error)
				<li>{{$error}}</li>
			@endforeach
		</ul>			  
	</div>
@endif