<form action="{{$blog->exists ? route('admin.update') : route('admin.create') }}" method="POST">
	{{csrf_field()}}
	@if($blog->exists)
		<input type="hidden" name="id" value="{{$blog->id}}">
	@endif
	<div class="form-group">
		<label for="title"></label>
		<input class="form-control" type="text" name="title" id="title" value="{{$blog->title}}">
	</div>
	<div class="form-group">
		<label for="body"></label>
		<textarea class="form-control" name="body" id="body">{{$blog->body}}</textarea>
	</div>

	@foreach($tags as $tag)
	<div class="form-check">
		<input {{$blog->tags->contains($tag->id) ? "checked" : ""}} type="checkbox" class="form-check-input" name="tags[]" id="tag-{{$tag->id}}" value="{{$tag->id}}">
		<label for="tag-{{$tag->id}}" class="form-check-label">{{$tag->name}}</label>
	</div>
	@endforeach
	<button class="btn btn-info" type="submit">Done</button>
</form>