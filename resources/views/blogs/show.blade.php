	@extends('layout.master')

@section('content')
	@include('partial.header')
	
	<div class="container">
		<h1 class="fancy-title">{{$blog->title}}</h1>
		<p>
			{{count($blog->likes)}} Likes | <a href="{{route('blog.create.like',['id' => $blog->id])}}">Like</a>
		</p>
		<p>{{$blog->body}}</p>
	</div>
@endsection