@extends('layout.master')


@section('content')

    @include('partial.header')
    
    <div class="container">
        <h1 class="fancy-title my-4">Media Indonesia</h1>
        @foreach($blogs as $blog)
            <div class="row text-md-left text-center">
                <div class="col-12">
                    <h2 class="text-secondary">{{$blog->title}}</h2>
                    <p class="text-truncate"">{{$blog->body}}</p>
                    @foreach($blog->tags as $tag)
                        <p class="d-inline">{{$tag->name}} |</p> 
                    @endforeach                  
                    <a href="{{route('blogs.show',['id' => $blog->id])}}">Read more...</a>
                </div>
            </div>
            <hr>
        @endforeach

        <div class="row">
            <div class="col">
                {{$blogs->links('vendor.pagination.bootstrap-4')}}
            </div>
        </div>
    </div>
    
    
@endsection