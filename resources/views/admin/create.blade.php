@extends('layout.master')

@section('content')
	@include('partial.header_admin')
	<div class="container">
		<h1 class="fancy-title">Create blog post</h1>
		@include('partial.error_message')
		@include('partial.form_blog')
	</div>
@endsection