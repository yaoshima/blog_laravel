@extends('layout.master')

@section('content')
	@include('partial.header_admin')
	<div class="container">
		<h2 class="fancy-title">Edit Blog Post</h2>
		@include('partial.form_blog')
	</div>
@endsection