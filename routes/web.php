<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', ['uses' => 'BlogController@index', 'as' => 'blogs.index']);

Route::get('/blog/{id}', ['uses' => 'BlogController@show', 'as' => 'blogs.show']);

Route::get('/about',function(){
	return view('other.about_us');
})
->name('other.about_us');



Route::get('/admin',['uses' => 'BlogController@adminIndex', 'as' => 'admin.index']);

Route::get('/admin/new', ['uses' => 'BlogController@new', 'as' => 'admin.new']);


Route::post('/admin', ['uses' => 'BlogController@create', 'as' => 'admin.create']);

Route::get('/admin/edit/{id}',['uses' => 'BlogController@edit', 'as' => 'admin.edit']);

Route::post('/admin/edit', ['uses' => 'BlogController@update', 'as' => 'admin.update']);

Route::get('/admin/delete/{id}', ['uses' => 'BlogController@destroy', 'as' => 'admin.destroy']);

Route::get('/admin/like/{id}', ['uses' => 'BlogController@createLike', 'as' => 'blog.create.like']);
